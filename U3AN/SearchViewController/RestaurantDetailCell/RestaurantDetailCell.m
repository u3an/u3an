//
//  RestaurantDetailCell.m
//  U3AN
//
//  Created by Vipin on 12/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantDetailCell.h"

@implementation RestaurantDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"RestaurantDetailCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (RestaurantDetailCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"RestaurantDetailCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (RestaurantDetailCell*)[nibViews objectAtIndex: 0];
        }
        
    }
    return self;
}


- (IBAction)detailButton_Action:(UIButton *)sender {
    [self.delegateRestaurantDetailItem detailButtonDidClicked:self];
}
@end
