//
//  AllRestaurantsVC.h
//  U3AN
//
//  Created by Vipin on 02/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantCell.h"

@interface AllRestaurantsVC : UIViewController<restaurantItemDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *restaurant_ScrollView;

@property(assign,nonatomic)BOOL isClickedOnBannerFlag;
@property (strong, nonatomic)NSString *bannerRestID;
@end
