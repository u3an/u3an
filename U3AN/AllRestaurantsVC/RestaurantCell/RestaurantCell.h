//
//  RestaurantCell.h
//  U3AN
//
//  Created by Vipin on 15/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"

@protocol restaurantItemDelegate;
@interface RestaurantCell : UIView
@property(weak,nonatomic)id<restaurantItemDelegate> delegateRestaurantItem;
@property (weak, nonatomic) IBOutlet UIImageView *restaurant_ImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName_Label;
@property (weak, nonatomic) IBOutlet UIButton *restaurantStatusButton;
@property (strong, nonatomic)Restaurant *restaurantObj;
@property (weak, nonatomic) IBOutlet UILabel *restaurantStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantSortOrderLabel;


@end

@protocol restaurantItemDelegate <NSObject>
- (void) restaurantItemDidClicked:(RestaurantCell *) restaurantCategoryItem;
@end