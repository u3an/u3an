//
//  RestaurantCell.m
//  U3AN
//
//  Created by Vipin on 15/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantCell.h"

@implementation RestaurantCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"RestaurantCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (RestaurantCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)restaurantDetailButtonAction:(UIButton *)sender
{
    [self.delegateRestaurantItem restaurantItemDidClicked:self];
}

@end
