//
//  DropDownView.h
//  DropDown
//
//  Created by Ratheesh Kumar R on 28/01/14.
//  Copyright (c) 2014 Ratheesh. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ListSelectionProtocol <NSObject>

-(void)selectList:(int)selectedIndex;

@end
@interface DropDownView : UIViewController
{
    id<ListSelectionProtocol> dropDownDelegate;
    
}
@property (strong, nonatomic) IBOutlet UITableView *dropDownTable;
@property (strong, nonatomic) IBOutlet UIImageView *dropDownBgImageView;

@property (strong, nonatomic) id<ListSelectionProtocol> dropDownDelegate;
@property(strong,nonatomic)NSMutableArray *dataArray;
@property(assign,nonatomic)CGFloat cellHeight;
@property(strong,nonatomic)UIFont *textLabelFont;
@property(strong,nonatomic)UIColor *textLabelColor;
@end
