//
//  SplashView.m
//  U3AN
//
//  Created by Ratheesh on 15/06/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SplashView.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
//#import <AVFoundation/AVFoundation.h>

@interface SplashView ()

@end

@implementation SplashView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"sounded_splash_black_bg_large" ofType:@"mp4"];
    
    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"sounded_splash_gray_bg" ofType:@"mp4"];
    
    
    NSURL* movieURL = [NSURL fileURLWithPath:moviePath];
    
    playerCtrl =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    //playerCtrl.scalingMode = MPMovieScalingModeFill;
    playerCtrl.controlStyle = MPMovieControlStyleNone;
    playerCtrl.view.frame = [[UIScreen mainScreen]bounds];
    
    //[playerCtrl setScalingMode:MPMovieScalingModeAspectFit];
    [playerCtrl setScalingMode:MPMovieScalingModeAspectFill];
    splashDefault = [NSUserDefaults standardUserDefaults];
    [splashDefault setObject:@"1" forKey:@"SplashFlag"];
    [splashDefault synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish1)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [self.view addSubview:playerCtrl.view];
    [playerCtrl play];
    
}

-(void) moviePlayBackDidFinish1{
    splashDefault = [NSUserDefaults standardUserDefaults];
    if ([[splashDefault objectForKey:@"SplashFlag"]isEqualToString:@"1"]) {
        [self loadMainViewMethod];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void) animationDidStart: (CAAnimation *) anim
{
    NSLog (@ "animation is start ...");
}
- (void)loadMainViewMethod
{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [ApplicationDelegate.window.layer addAnimation:transition forKey:nil];
    
    if (ApplicationDelegate.window.rootViewController) {
        ApplicationDelegate.window.rootViewController = nil;
    }
    
    ApplicationDelegate.window.rootViewController=ApplicationDelegate.homeNav;
    
}

@end
