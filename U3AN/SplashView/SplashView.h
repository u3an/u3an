//
//  SplashView.h
//  U3AN
//
//  Created by Ratheesh on 15/06/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface SplashView : UIViewController{
    NSMutableArray * splashAnimationImageArray;
    //int flag;
    MPMoviePlayerController * playerCtrl;
    NSUserDefaults * splashDefault;
}

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *splashAnimationView;

@end
