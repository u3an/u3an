//
//  DropDownWithHeaderSelection.m
//  U3AN
//
//  Created by Vipin on 09/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "DropDownWithHeaderSelection.h"

@interface DropDownWithHeaderSelection ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation DropDownWithHeaderSelection
@synthesize headerDropDownDelegate,dataArray,headerDataArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // Code comes here..
    }
    
    NSArray *array = [dataArray objectAtIndex:indexPath.section];
    cell.textLabel.text =[NSString stringWithFormat:@"%@",[array objectAtIndex:indexPath.row]];
    //    cell.textLabel.textColor = [UIColor grayColor];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    
    cell.backgroundColor = [UIColor clearColor];
    //    cell.backgroundColor = [UIColor colorWithRed:(245.0f/255.0f) green:(245.0f/255.0f) blue:(245.0f/255.0f) alpha:1.0];
    
    //    UIImageView * separatorLineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 267, 1)];
    //    separatorLineView.backgroundColor = [UIColor clearColor];
    //    separatorLineView.image = [UIImage imageNamed:@"line_black.png"];
    //    [cell.contentView addSubview:separatorLineView];
    
    //    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    [cell.textLabel setFont:self.textLabelFont];
    [cell.textLabel setTextColor:self.textLabelColor];
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentRight;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(5, 0, (tableView.frame.size.width-10), 50.0)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    if ([ApplicationDelegate isValid:headerDataArray])
    {
        if (headerDataArray.count>0)
        {
            if (headerDataArray.count>section)
            {
                if ([ApplicationDelegate isValid:[headerDataArray objectAtIndex:section]])
                {
                    MarqueeLabel *nameLabel = [[MarqueeLabel alloc] initWithFrame:CGRectMake(3, 0, (sectionHeaderView.frame.size.width-6), 25.0)];
                    
                    nameLabel.marqueeType = MLContinuous;
                    NSString *lblTxt = [NSString stringWithFormat:@"%@",[headerDataArray objectAtIndex:section]];
                    
                    CGSize messageSize = [lblTxt sizeWithAttributes:@{NSFontAttributeName:self.headerLabelFont}];
                    float duration = messageSize.width / 20;
                    nameLabel.scrollDuration = duration;
                    
                    nameLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
                    nameLabel.fadeLength = 10.0f;
                    nameLabel.continuousMarqueeExtraBuffer = 10.0f;
                    nameLabel.textColor=self.headerTextLabelColor;
                    nameLabel.font = self.headerLabelFont;
                    nameLabel.text = lblTxt;
                    nameLabel.textAlignment = NSTextAlignmentCenter;
                    nameLabel.backgroundColor = [UIColor clearColor];
                    [sectionHeaderView addSubview:nameLabel];
                }
            }
        }
    }
    

    
    /*
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(3, 0, (sectionHeaderView.frame.size.width-6), 25.0)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:self.headerLabelFont];
    headerLabel.textColor = self.headerTextLabelColor;
    [sectionHeaderView addSubview:headerLabel];
    headerLabel.text =[NSString stringWithFormat:@"%@",[headerDataArray objectAtIndex:section]];
     */
    
    return sectionHeaderView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[dataArray objectAtIndex:section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight;
}
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *view = [[UIView alloc] init];
//    view.frame=CGRectMake(10, 10, 1, 30);
//    view.backgroundColor = [UIColor redColor];
//    return view;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return headerDataArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.headerDropDownDelegate headerSelectList:indexPath.row :indexPath.section];
}


@end
