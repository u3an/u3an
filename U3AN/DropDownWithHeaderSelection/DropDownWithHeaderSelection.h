//
//  DropDownWithHeaderSelection.h
//  U3AN
//
//  Created by Vipin on 09/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ListWithHeaderSelectionProtocol <NSObject>

-(void)headerSelectList:(int)selectedIndex :(NSInteger)section;

@end
@interface DropDownWithHeaderSelection : UIViewController
{
    id<ListWithHeaderSelectionProtocol> headerDropDownDelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *dropDownWithSelectionTable;
@property (strong, nonatomic) id<ListWithHeaderSelectionProtocol> headerDropDownDelegate;
@property(strong,nonatomic)NSMutableArray *dataArray;
@property(strong,nonatomic)NSMutableArray *headerDataArray;
@property(assign,nonatomic)CGFloat cellHeight;
@property(strong,nonatomic)UIFont *textLabelFont;
@property(strong,nonatomic)UIFont *headerLabelFont;
@property(strong,nonatomic)UIColor *textLabelColor;
@property(strong,nonatomic)UIColor *headerTextLabelColor;

@end


