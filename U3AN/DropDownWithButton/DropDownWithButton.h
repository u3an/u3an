//
//  DropDownWithButton.h
//  U3AN
//
//  Created by Vipin on 27/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ListWithSelectionButtonProtocol <NSObject>

-(void)buttonSelectList:(int)selectedIndex;

@end


@interface DropDownWithButton : UIViewController
{
      id<ListWithSelectionButtonProtocol> selectionBttnDropDownDelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *dropDownWithSelectionButtonTable;
@property (strong, nonatomic) id<ListWithSelectionButtonProtocol> selectionBttnDropDownDelegate;
@property(strong,nonatomic)NSMutableArray *dataArray;
@property(strong,nonatomic)NSMutableArray *headerDataArray;
@property(assign,nonatomic)CGFloat cellHeight;
@property(strong,nonatomic)UIFont *textLabelFont;
@property(strong,nonatomic)UIColor *textLabelColor;

@end
