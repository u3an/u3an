//
//  RestaurantMenuListVC.h
//  U3AN
//
//  Created by Vipin on 24/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantMenuListVC : UIViewController
@property (strong, nonatomic) IBOutlet UIView *contentContainerView;

@property (strong, nonatomic) IBOutlet UILabel *restuarantMenuHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *reviewsLabel;
@property (strong, nonatomic) IBOutlet UIView *ratingsView;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;

@property (strong, nonatomic) IBOutlet UITableView *restaurantMenuTable;
@property (strong, nonatomic) IBOutlet UIView *restuntmenuheader;

@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (assign, nonatomic)NSInteger rating_Count;

- (IBAction)review_ButtonAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *menutopBackground;

@end
