//
//  RestaurantMenuCell.h
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DishMenuDetails.h"

@protocol menuItemDelegate;
@interface RestaurantMenuCell : UIView
@property(weak,nonatomic)id<menuItemDelegate> delegateMenuItem;

@property(nonatomic,retain)DishMenuDetails *dishesMenuItemObject;

@property (weak, nonatomic) IBOutlet UIImageView *dish_ImageView;
@property (weak, nonatomic) IBOutlet UILabel *dish_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *dish_Discription_Label;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *price_Label;
@property (weak, nonatomic) IBOutlet UIButton *menu_Choice_Button;
@property (strong, nonatomic)NSMutableArray *choicesSelected;
@property (weak, nonatomic) IBOutlet UITextField *quantity_TxtBox;
@property (strong, nonatomic) IBOutlet UIButton *addFavButton;

- (IBAction)order_ButtonAction:(UIButton *)sender;
- (IBAction)menuChoice_ButtonAction:(UIButton *)sender;

- (IBAction)minusButton:(id)sender;

- (IBAction)plusButton:(id)sender;


@end

@protocol menuItemDelegate <NSObject>
- (void) orderButtonDidClicked:(RestaurantMenuCell *) restaurantMenuItem;
- (void) menuChoiceButtonDidClicked:(RestaurantMenuCell *) restaurantMenuItem;
- (void) addFavButtonDidClicked:(RestaurantMenuCell *) restaurantMenuItem;

- (void) minusButtonDidClicked:(RestaurantMenuCell *) restaurantMenuItem;

@end