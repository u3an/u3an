//
//  RestaurantMenuDetailVC.h
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantMenuCell.h"
#import "DishMenuDetails.h"
#import "DropDownWithButton.h"

@interface RestaurantMenuDetailVC : UIViewController<menuItemDelegate,ListWithSelectionButtonProtocol,UITextFieldDelegate>

@property (strong, nonatomic) DropDownWithButton *restaurantMenuChoiceDropDownObj;

@property (weak, nonatomic) IBOutlet UILabel *restaurant_Menu_Label;
@property (weak, nonatomic) IBOutlet UIImageView *restaurant_ImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *review_Label;
@property (weak, nonatomic) IBOutlet UILabel *menu_Item_Label;
@property (weak, nonatomic) IBOutlet UIScrollView *dishes_ScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *restaurant_Menu_ScrollView;
@property (weak, nonatomic) IBOutlet UIView *rating_View;
@property (strong, nonatomic)RestaurantMenuSection *restaurantMenuObj;
@property (assign, nonatomic)NSInteger rating_Count;
@property (assign, nonatomic)NSInteger restaurant_rating_Count;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (strong, nonatomic)RestaurantMenuCell *restaurantMenuCellObj;
- (IBAction)review_ButtonAction:(UIButton *)sender;

//****************** MENU CHOICE POP_UP ***************************

@property (strong, nonatomic) IBOutlet UIView *menuChoicePopUpView;
@property (strong, nonatomic) IBOutlet UIView *menuChoicePopUpContentView;
@property (strong, nonatomic) IBOutlet UITableView *menuChoicePopUpTableView;

- (IBAction)menuChoicePopUpDismissAction:(id)sender;

//************************ GUEST USER POP_UP **************************

@property (strong, nonatomic) IBOutlet UIView *guestUserInnerView;
@property (strong, nonatomic) IBOutlet UIView *guestUserPopUpView;

- (IBAction)guestPopUpViewDismissAction:(UIButton *)sender;
- (IBAction)continueAsGuestBttnAction:(UIButton *)sender;
- (IBAction)loginBttnAction:(UIButton *)sender;

@end
