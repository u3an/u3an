//
//  RestaurantReviewCell.m
//  U3AN
//
//  Created by Vipin on 26/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "RestaurantReviewCell.h"

@implementation RestaurantReviewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"RestaurantReviewCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (RestaurantReviewCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}
@end
