//
//  RestaurantReviewCell.h
//  U3AN
//
//  Created by Vipin on 26/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantReviewCell : UIView
@property (weak, nonatomic) IBOutlet UITextView *review_TextView;
@property (weak, nonatomic) IBOutlet UILabel *tmpLabel;

@end
