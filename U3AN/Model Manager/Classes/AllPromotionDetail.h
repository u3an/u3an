//
//  AllPromotionDetail.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllPromotionDetail : NSObject
@property(nonatomic,retain)NSString *promo_Description;
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *price;
@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *thumbnail;

@end
