//
//  Restaurant.h
//  U3AN
//
//  Created by Vipin on 15/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Restaurant : NSObject

@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *restaurant_Logo;
@property(nonatomic,retain)NSString *restaurant_Name;
@property(nonatomic,retain)NSString *restaurant_Status;
@property(nonatomic,retain)NSString *summary;
@property(nonatomic,retain)NSString *positionPrice;
@property(nonatomic,retain)NSString *SortOrder;
@end
