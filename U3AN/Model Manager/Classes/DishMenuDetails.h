//
//  DishMenuDetails.h
//  U3AN
//
//  Created by Vipin on 25/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DishMenuDetails : NSObject

@property(nonatomic,retain)NSString *description;
@property(nonatomic,retain)NSString *discount;
@property(nonatomic,retain)NSString *menuItemId;
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *price;
@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *dish_Thumbnail;
@property(nonatomic,retain)NSMutableArray *menuChoice_array;

@end
