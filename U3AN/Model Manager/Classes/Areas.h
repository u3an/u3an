//
//  Areas.h
//  U3AN
//
//  Created by Vipin on 22/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Areas : NSObject
@property(nonatomic,retain)NSMutableArray *area_List;
@property(nonatomic,retain)NSString *city_Name;
@property(nonatomic,retain)NSString *city_Id;

@end
