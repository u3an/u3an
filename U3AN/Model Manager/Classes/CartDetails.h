//
//  CartDetails.h
//  U3AN
//
//  Created by Vipin on 06/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartDetails : NSObject
@property(nonatomic,retain)NSString *area_Id;
@property(nonatomic,retain)NSString *item_ChoiceId;
@property(nonatomic,retain)NSString *item_Id;
@property(nonatomic,retain)NSString *item_Name;
@property(nonatomic,retain)NSString *item_Price;
@property(nonatomic,retain)NSString *item_Thumbnail;
@property(nonatomic,retain)NSString *order_Date;
@property(nonatomic,retain)NSString *quantity;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *shopping_CartId;


@end
