//
//  MyOrderItemDetail.h
//  U3AN
//
//  Created by Vipin on 09/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderItemDetail : NSObject

@property(nonatomic,retain)NSString *item_ID;
@property(nonatomic,retain)NSString *item_Name;
@property(nonatomic,retain)NSString *item_Price;
@property(nonatomic,retain)NSString *item_Quantity;
@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *special_Request;
@property(nonatomic,retain)NSString *status;
@property(nonatomic,retain)NSMutableArray *itemChoice_Array;

@end
