//
//  PageDetails.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageDetails : NSObject
@property(nonatomic,retain)NSString *page_Data;
@property(nonatomic,retain)NSString *status;

@end
