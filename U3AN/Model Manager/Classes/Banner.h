//
//  Banner.h
//  U3AN
//
//  Created by Ratheesh on 04/06/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Banner : NSObject
@property(nonatomic,retain)NSString *bannerImageUrl;
@property(nonatomic,retain)NSString *restaurantID;

@end
