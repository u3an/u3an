//
//  Dishes.h
//  U3AN
//
//  Created by Vipin on 19/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dishes : NSObject

@property(nonatomic,retain)NSString *dish_Name;
@property(nonatomic,retain)NSString *dish_Id;
@property(nonatomic,retain)NSString *dish_Price;
@property(nonatomic,retain)NSString *dish_Description;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *restaurant_Name;
@property(nonatomic,retain)NSString *restaurent_Status;
@property(nonatomic,retain)NSString *dish_Thumbnail;
@property(nonatomic,retain)NSString *restaurant_Thumbnail;

@end
