//
//  MyOrderDeliveryDetail.h
//  U3AN
//
//  Created by Vipin on 09/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderDeliveryDetail : NSObject

@property(nonatomic,retain)NSString *order_Date;
@property(nonatomic,retain)NSString *delivery_AddressDirection;
@property(nonatomic,retain)NSString *delivery_AddressType;
@property(nonatomic,retain)NSString *delivery_Area;
@property(nonatomic,retain)NSString *delivery_Block;
@property(nonatomic,retain)NSString *delivery_BuildingNo;
@property(nonatomic,retain)NSString *delivery_Floor;
@property(nonatomic,retain)NSString *delivery_Judda;
@property(nonatomic,retain)NSString *delivery_Mobile;
@property(nonatomic,retain)NSString *delivery_Street;
@property(nonatomic,retain)NSString *delivery_Suit;
@property(nonatomic,retain)NSString *paid_ByCC;
@property(nonatomic,retain)NSString *paid_ByCash;
@property(nonatomic,retain)NSString *paid_ByKNET;
@property(nonatomic,retain)NSString *paid_Byu3an;
@property(nonatomic,retain)NSString *total;
@property(nonatomic,retain)NSString *transactionID;
@property(nonatomic,retain)NSString *userIP;
@property(nonatomic,retain)NSMutableArray *orders_Array;

@end
