//
//  CustomerAddressDetails.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CustomerAddressDetails.h"

@implementation CustomerAddressDetails

@synthesize areaID,areaName,birthday,block,buildingNo,extraDirections,firstName,floor,gender,housePhone,addressID,isPrimary,judda,lastName,mobile,profileName,street,suit,type,workPhone,company,email,occupation,subscribed_to_news,subscribed_to_sms;

@end
