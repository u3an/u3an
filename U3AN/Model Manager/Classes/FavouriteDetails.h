//
//  FavouriteDetails.h
//  U3AN
//
//  Created by Vipin on 26/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavouriteDetails : NSObject

@property(nonatomic,retain)NSString *fav_Description;
@property(nonatomic,retain)NSString *itemID;
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *price;
@property(nonatomic,retain)NSString *status;
@property(nonatomic,retain)NSString *thumbnail;


@end
