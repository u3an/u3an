//
//  MyOrderOrdersDetail.h
//  U3AN
//
//  Created by Vipin on 09/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOrderOrdersDetail : NSObject

@property(nonatomic,retain)NSString *charges_ForU3an;
@property(nonatomic,retain)NSString *delivery_Charges;
@property(nonatomic,retain)NSString *delivery_Time;
@property(nonatomic,retain)NSString *discount;
@property(nonatomic,retain)NSString *general_Request;
@property(nonatomic,retain)NSString *order_ID;
@property(nonatomic,retain)NSString *order_Mode;
@property(nonatomic,retain)NSString *order_Source;
@property(nonatomic,retain)NSString *order_Status;
@property(nonatomic,retain)NSString *order_Type;
@property(nonatomic,retain)NSString *payment_Method;
@property(nonatomic,retain)NSString *rating;
@property(nonatomic,retain)NSString *restaurant;
@property(nonatomic,retain)NSString *restaurant_ID;
@property(nonatomic,retain)NSString *subTotal;
@property(nonatomic,retain)NSString *total;
@property(nonatomic,retain)NSMutableArray *orderItems_Array;

@end
