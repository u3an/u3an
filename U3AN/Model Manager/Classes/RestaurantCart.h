//
//  RestaurantCart.h
//  U3AN
//
//  Created by Vipin on 09/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantCart : NSObject

@property(nonatomic,retain)NSString *restaurant_Name;
@property(nonatomic,retain)NSString *restaurant_Thumbnail;
@property(nonatomic,retain)NSString *minimum_Amount;
@property(nonatomic,retain)NSString *restaurant_Id;
@property(nonatomic,retain)NSString *accepts_CC;
@property(nonatomic,retain)NSString *accepts_Cash;
@property(nonatomic,retain)NSString *accepts_KNET;
@property(nonatomic,retain)NSString *delivery_Charges;
@property(nonatomic,retain)NSString *discount;
@property(nonatomic,retain)NSString *restaurant_Total;
@property(nonatomic,retain)NSString *subtotal;
@property(nonatomic,retain)NSMutableArray *cart_InfoArray;
@property(nonatomic,retain)NSMutableArray *delivery_TimingArray;

@end
