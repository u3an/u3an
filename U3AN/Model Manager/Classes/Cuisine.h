//
//  Cuisine.h
//  U3AN
//
//  Created by Vipin on 20/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cuisine : NSObject

@property(nonatomic,retain)NSString *cuisine_Name;
@property(nonatomic,retain)NSString *cuisine_Id;

@end
