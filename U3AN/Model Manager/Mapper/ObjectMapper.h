//
//  ObjectMapper.h
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Restaurant.h"
#import "Dishes.h"
#import "Cuisine.h"
#import "AreasByRestaurant.h"
#import "RestaurantDetail.h"
#import "UserData.h"
#import "Promotions.h"
#import "Areas.h"
#import "RestaurantMenuSection.h"
#import "DishMenuDetails.h"
#import "RestaurantReview.h"
#import "CartDetails.h"
#import "RestaurantCart.h"
#import "PageDetails.h"
#import "AllPromotionDetail.h"
#import "AllRestaurantPromotion.h"
#import "GiftVoucher.h"
#import "CustomerAddressDetails.h"
#import "FavouriteDetails.h"
#import "CartInformation.h"
#import "MyOrderDeliveryDetail.h"
#import "MyOrderOrdersDetail.h"
#import "MyOrderItemDetail.h"
#import "Banner.h"

@interface ObjectMapper : NSObject

-(Banner *)getBannerObjectFromDictionary:(NSMutableDictionary *)bannerDictionary;

-(Restaurant *)getRestaurantListFromDictionary:(NSMutableDictionary *)restaurantDictionary;
-(Dishes *)getMostSellingDishesListFromDictionary:(NSMutableDictionary *)dishesDictionary;
-(Cuisine *)getMostSellingCusineListFromDictionary:(NSMutableDictionary *)cusineDictionary;
-(AreasByRestaurant *)getAreaByRestaurantListFromDictionary:(NSMutableDictionary *)areaDictionary;
-(Areas *)getAreaListFromDictionary:(NSMutableDictionary *)areaDictionary;
-(RestaurantDetail *)getRestaurantDetailsFromDictionary:(NSMutableDictionary *)restaurantDictionary;
-(RestaurantDetail *)getRestaurantDetailsForORDER_DISHES_FromDictionary:(NSMutableDictionary *)restaurantDictionary;
-(RestaurantMenuSection *)getRestaurantMenuSectionObjectFromDictionary:(NSMutableDictionary *)restaurantDictionary;
-(UserData *) getUserDataFromDictionary:(NSMutableDictionary *)userDictionary;
-(Promotions *) getRestaurantsByPromotionsListFromDictionary:(NSMutableDictionary *)promotionsDictionary;
-(DishMenuDetails *) getDishMenuDetailFromDictionary:(NSMutableDictionary *)dishMenuDetailDictionary;
-(RestaurantReview *) getRestaurantReviewDetailFromDictionary:(NSMutableDictionary *)restaurantDetailDictionary;
-(CartInformation *) getCartInfoListFromDictionary:(NSMutableDictionary *)cartInfoDictionary;
-(RestaurantCart *) getRestaurantCartListFromDictionary:(NSMutableDictionary *)restaurantCartDictionary;
-(CartDetails *) getCartDetailFromDictionary:(NSMutableDictionary *)cartDetailDictionary;
-(PageDetails *) getPageDetailFromDictionary:(NSMutableDictionary *)pageDetailDictionary;
-(AllPromotionDetail *) getPromotionDetailFromDictionary:(NSMutableDictionary *)promotionDetailDictionary;
-(AllRestaurantPromotion *) getAllRestaurantPromotionFromDictionary:(NSMutableDictionary *)allRestaurantDictionary;
-(GiftVoucher *) getGiftVoucherDetailsFromDictionary:(NSMutableDictionary *)giftVoucherDictionary;
-(CustomerAddressDetails *) getCustAddressDetailsFromDictionary:(NSMutableDictionary *)custAddressDictionary;
-(FavouriteDetails *) getFavouriteDetailsFromDictionary:(NSMutableDictionary *)favouriteDictionary;
-(MyOrderDeliveryDetail *)getOrderDeliveryDetailsFromDictionary:(NSMutableDictionary *)orderDeliveryDictionary;
-(MyOrderOrdersDetail *)getOrderDetailsFromDictionary:(NSMutableDictionary *)orderDetailDictionary;
-(MyOrderItemDetail *)getOrderItemDetailsFromDictionary:(NSMutableDictionary *)orderItemDetailDictionary;

@end
