//
//  AccountInformationVC.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountInformationVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *accountInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *lastNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *mobileLbl;
@property (strong, nonatomic) IBOutlet UILabel *houseNumberLbl;
@property (strong, nonatomic) IBOutlet UIButton *saveBttn;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *mobileTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *houseNumberTxtBox;
@property (strong, nonatomic) IBOutlet UIScrollView *accountInfoScrollView;
@property (strong, nonatomic) NSMutableArray *custAddressListArray;

- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)saveButtonAction:(UIButton *)sender;

@end
