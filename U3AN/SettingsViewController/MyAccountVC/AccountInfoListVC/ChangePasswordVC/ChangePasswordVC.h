//
//  ChangePasswordVC.h
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *changPsswdScrollView;
@property (strong, nonatomic) IBOutlet UILabel *changePasswordLbl;
@property (strong, nonatomic) IBOutlet UILabel *oldPasswordLbl;
@property (strong, nonatomic) IBOutlet UILabel *unewPasswordLbl;
@property (strong, nonatomic) IBOutlet UILabel *reTypeNewPassLbl;
@property (strong, nonatomic) IBOutlet UITextField *oldPasswdTxtBox;
@property (strong, nonatomic) IBOutlet UITextField *uNewPasswdTxt;
@property (strong, nonatomic) IBOutlet UITextField *reNewPasswdTxt;
@property (strong, nonatomic) IBOutlet UIButton *changePassBttn;

- (IBAction)changePasswdBttnAction:(UIButton *)sender;
- (IBAction)cancelBttnAction:(UIButton *)sender;

@end
