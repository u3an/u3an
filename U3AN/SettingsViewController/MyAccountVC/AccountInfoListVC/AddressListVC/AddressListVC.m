//
//  AddressListVC.m
//  U3AN
//
//  Created by Vipin on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AddressListVC.h"
#import "AddressListCell.h"
#import "AddressListCell_a.h"
#import "CustomerAddressDetails.h"
#import "AddAddressVC.h"
#import "LaunchingViewController.h"

@interface AddressListVC ()

@end

@implementation AddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.addressListLbl.text = localize(@"Address List");
    if ([ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        self.addressListLbl.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        self.addressListLbl.textAlignment = NSTextAlignmentRight;
    }
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (ApplicationDelegate.isLoggedIn)
    {
    [self updateViewHeading];
    [self setupUI];
    [self getCustomerAddresses];
    }
    else
    {
        [self loadHomeView];
    }
    
}

-(void)loadHomeView
{
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    for (UIView *vw in [HomeTabViewController sharedViewController].containerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    [HomeTabViewController sharedViewController].headerView.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=YES;
    LaunchingViewController *launchingVc = [[LaunchingViewController alloc] initWithNibName:@"LaunchingViewController" bundle:nil];
    launchingVc.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav = [[UINavigationController alloc] initWithRootViewController:launchingVc];
    ApplicationDelegate.subHomeNav.navigationBar.translucent = NO;
    ApplicationDelegate.subHomeNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.subHomeNav.navigationBarHidden=YES;
    ApplicationDelegate.subHomeNav.view.frame=CGRectMake(0, 0, [HomeTabViewController sharedViewController].containerView.frame.size.width, [HomeTabViewController sharedViewController].containerView.frame.size.height);
    [[HomeTabViewController sharedViewController].containerView addSubview:ApplicationDelegate.subHomeNav.view];
    
}
-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}
-(void)setupUI
{
    self.addressListScrollView.layer.cornerRadius = 3.0;
    self.addressListScrollView.layer.masksToBounds = YES;
    [self.addressListLbl setFont:[UIFont fontWithName:@"BREESERIF-REGULAR" size:18.0f]];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSMutableDictionary *)getAddressPostData:(NSIndexPath *)path
{
      CustomerAddressDetails *adressItem=[self.custAddressListArray objectAtIndex:path.row];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:adressItem.addressID forKey:@"AddressId"];
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"UserId"];
    
    
    return postDic;
}


-(void)getCustomerAddresses
{
    self.custAddressListArray= [[NSMutableArray alloc] init];
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (custPostData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             NSMutableArray *addressArray = [[NSMutableArray alloc] init];
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                     {
                         
                         if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                         {
                             
                             
                             if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                             {
                                 addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                             }
                             if (addressArray.count>0)
                             {
                                 for (int i=0; i<addressArray.count; i++)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:i]])
                                     {
                                         CustomerAddressDetails *addressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:i]];
                                         
                                         [self.custAddressListArray addObject:addressItem];
                                     }
                                     
                                 }
                                 
                                 [self.addressListTable reloadData] ;
                             }
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                         }
                     }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}


#pragma mark - UITABLEVIEW METHODS

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *addressTableIdentifier = @"addressListCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
 
    if (cell == nil)
    {
        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            [tableView registerNib:[UINib nibWithNibName:@"AddressListCell" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
        else
        {
            [tableView registerNib:[UINib nibWithNibName:@"AddressListCell_a" bundle:nil] forCellReuseIdentifier:addressTableIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:addressTableIdentifier];
        }
    }
    
    CustomerAddressDetails *adressItem=[self.custAddressListArray objectAtIndex:indexPath.row];
    UILabel *lbl_text=(UILabel *)[cell viewWithTag:1];
    lbl_text.text=adressItem.street;
    lbl_text.font = [UIFont fontWithName:@"Tahoma" size:13.0f];
    UIButton *cell_button=(UIButton *)[cell viewWithTag:2];
    [cell_button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *make_as_PrimaryButton=(UIButton*)[cell viewWithTag:3];
    [make_as_PrimaryButton addTarget:self action:@selector(makeAsPrimaryButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    if ([adressItem.isPrimary isEqualToString:[NSString stringWithFormat:@"True"]]) {
        cell_button.hidden=YES;
        make_as_PrimaryButton.hidden=YES;
    }
    else
    {
        cell_button.hidden=NO;
        make_as_PrimaryButton.hidden=NO;

    }
    
   // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


- (void)checkButtonTapped:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.addressListTable];
    NSIndexPath *indexPath = [self.addressListTable indexPathForRowAtPoint: currentTouchPosition];
    
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddressPostData:indexPath]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine removeAddressWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                   //  [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                     
                     [self getCustomerAddresses];
                    // [self.addressListTable reloadData];

                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }

    

}

- (void)makeAsPrimaryButtonTapped:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.addressListTable];
    NSIndexPath *indexPath = [self.addressListTable indexPathForRowAtPoint: currentTouchPosition];
    
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getAddressPostData:indexPath]];
    if (postData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine makeAsPrimaryAddressWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                 {
                    // [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                     
                     [self getCustomerAddresses];
                     // [self.addressListTable reloadData];
                     
                     
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.custAddressListArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddAddressVC *addAddressVC = [[AddAddressVC alloc] initWithNibName:@"AddAddressVC" bundle:nil]
    ;
    addAddressVC.view.backgroundColor = [UIColor clearColor];
     addAddressVC.customerDetailObj=[self.custAddressListArray objectAtIndex:indexPath.row];
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[addAddressVC class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:addAddressVC animated:NO];
    }
    
}


@end
