//
//  OrderlistViewController.h
//  U3AN
//
//  Created by Vineeth on 01/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderlistViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *orderListTable;
@property (strong, nonatomic) NSMutableArray *orderListArray;
@end
