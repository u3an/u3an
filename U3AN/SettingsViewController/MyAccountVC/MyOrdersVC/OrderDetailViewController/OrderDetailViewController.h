//
//  OrderDetailViewController.h
//  U3AN
//
//  Created by Vineeth on 03/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"

@interface OrderDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,DLStarRatingDelegate>

@property (strong, nonatomic) IBOutlet UILabel *myOrdersHeaderLabel;
@property (strong, nonatomic) IBOutlet UITableView *orderListTable;
@property (strong, nonatomic) NSMutableArray *orderDetailListArray;
@property (strong, nonatomic) NSString *passtransactionid;
@property (strong, nonatomic) IBOutlet UILabel *header_date_lbl;
@property (strong, nonatomic) IBOutlet UILabel *header_area_lbl;
@property (strong, nonatomic) IBOutlet UILabel *header_price_lbl;
@property(nonatomic, retain) UILabel *rateLabel;
@end
