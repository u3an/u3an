//
//  SettingsViewController.h
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *settingsInnerView;
@property (strong, nonatomic) IBOutlet UIView *settingsInnerViewForNotLogin;
@property (strong, nonatomic) IBOutlet UILabel *settingsLbl;

- (IBAction)myAccountBttnAction:(UIButton *)sender;
- (IBAction)logoutButtonAction:(UIButton *)sender;
- (IBAction)logingButtonAction:(UIButton *)sender;
- (IBAction)signUpButtonAction:(UIButton *)sender;
- (IBAction)settingDismissAction:(UIButton *)sender;

@end
