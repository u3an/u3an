//
//  MostSellingDishByResCell.h
//  U3AN
//
//  Created by Vipin on 29/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dishes.h"

@protocol dishesByResItemDelegate;

@interface MostSellingDishByResCell : UIView
@property(weak,nonatomic)id<dishesByResItemDelegate> delegateDishesByResItem;
@property (weak, nonatomic) IBOutlet UILabel *dishName_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurantStatus_Label;
@property (weak, nonatomic) IBOutlet UIImageView *mostSellingDish_ImageView;
@property (strong, nonatomic)Dishes *dishesObj;

@end

@protocol dishesByResItemDelegate <NSObject>
- (void) dishesByResItemDidClicked:(MostSellingDishByResCell *) dishesByResCategoryItem;
@end