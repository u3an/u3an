//
//  MostSellingDishByResCell.m
//  U3AN
//
//  Created by Vipin on 29/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MostSellingDishByResCell.h"

@implementation MostSellingDishByResCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"MostSellingDishByResCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (MostSellingDishByResCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)dishByResDetailButtonAction:(UIButton *)sender
{
    [self.delegateDishesByResItem dishesByResItemDidClicked:self];
}


@end
