//
//  MostSellingDishesByCuisinesVC.h
//  U3AN
//
//  Created by Vipin on 30/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MostSellingDishByResCell.h"
#import "Cuisine.h"

@interface MostSellingDishesByCuisinesVC : UIViewController<dishesByResItemDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mostSellingDishesByCuisine_ScrollView;
@property (strong, nonatomic)Cuisine *cuisineObj;


@end
