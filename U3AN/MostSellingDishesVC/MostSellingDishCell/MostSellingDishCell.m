//
//  MostSellingDishCell.m
//  U3AN
//
//  Created by Vipin on 02/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MostSellingDishCell.h"

@implementation MostSellingDishCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"MostSellingDishCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (MostSellingDishCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)dishDetailButtonAction:(UIButton *)sender
{
    [self.delegateDishesItem dishesItemDidClicked:self];
}

@end
