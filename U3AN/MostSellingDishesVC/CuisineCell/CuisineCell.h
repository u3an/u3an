//
//  CuisineCell.h
//  U3AN
//
//  Created by Vipin on 21/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cuisine.h"

@protocol cuisineItemDelegate;
@interface CuisineCell : UIView
@property(weak,nonatomic)id<cuisineItemDelegate> delegateCuisineItem;
@property (weak, nonatomic) IBOutlet UIImageView *cuisineCellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cuisineTitleLabel;
@property (strong, nonatomic)Cuisine *cuisineObj;

@end

@protocol cuisineItemDelegate <NSObject>
- (void) cuisineItemDidClicked:(CuisineCell *) cuisineCategoryItem;
@end