//
//  CuisineCell.m
//  U3AN
//
//  Created by Vipin on 21/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CuisineCell.h"

@implementation CuisineCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"CuisineCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (CuisineCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)cuisineDetailButtonAction:(UIButton *)sender
{
    [self.delegateCuisineItem cuisineItemDidClicked:self];
}

@end
