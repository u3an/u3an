//
//  MostSellingDishesVC.h
//  U3AN
//
//  Created by Vipin on 31/12/14.
//  Copyright (c) 2014 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantCell.h"
#import "MostSellingDishCell.h"
#import "CuisineCell.h"

@interface MostSellingDishesVC : UIViewController<restaurantItemDelegate,dishesItemDelegate,cuisineItemDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mostSellingScrollView;
@property (weak, nonatomic) IBOutlet UILabel *mostlabel;

@end
