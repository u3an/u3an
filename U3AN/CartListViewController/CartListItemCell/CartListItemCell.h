//
//  CartListItemCell.h
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartDetails.h"

@protocol cartListItemDelegate;

@interface CartListItemCell : UIView
@property(weak,nonatomic)id<cartListItemDelegate> delegateCartListItem;
@property (strong, nonatomic) IBOutlet UIImageView *cuisineImageView;
@property (strong, nonatomic) IBOutlet UILabel *orderItemLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderItemPriceLabel;
@property (strong, nonatomic) IBOutlet UIButton *removeOrderItemButton;
@property (strong, nonatomic)CartDetails *cartDetailObj;

- (IBAction)deleteOrderItemAction:(id)sender;

@end

@protocol cartListItemDelegate <NSObject>

- (void) cartListItemDidClickedForRemoval:(CartListItemCell *) cartListCategoryItem;

@end