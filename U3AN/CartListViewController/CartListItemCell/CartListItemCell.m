//
//  CartListItemCell.m
//  U3AN
//
//  Created by Vipin on 05/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CartListItemCell.h"

@implementation CartListItemCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"CartListItemCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (CartListItemCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"CartListItemCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (CartListItemCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}


- (IBAction)deleteOrderItemAction:(id)sender {
    [self.delegateCartListItem cartListItemDidClickedForRemoval:self];
}
@end
