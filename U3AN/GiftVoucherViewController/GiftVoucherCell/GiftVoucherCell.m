//
//  GiftVoucherCell.m
//  U3AN
//
//  Created by Vipin on 19/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "GiftVoucherCell.h"

@implementation GiftVoucherCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"GiftVoucherCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (GiftVoucherCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)giftVoucherDetailButtonAction:(UIButton *)sender
{
    [self.delegategiftVoucherItem giftVoucherItemDidClicked:self];
}


@end
