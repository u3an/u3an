//
//  LoginViewController.h
//  U3AN
//
//  Created by Vipin on 03/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextBox;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextBox;
@property (weak, nonatomic) IBOutlet UIButton *keepLoggedInBttn;
@property (weak, nonatomic) IBOutlet UIScrollView *login_ScrollView;
@property (strong, nonatomic) IBOutlet UILabel *name_Label;
@property (strong, nonatomic) IBOutlet UILabel *password_Label;
@property (strong, nonatomic) IBOutlet UILabel *keepLoggedIn_Label;
@property (weak, nonatomic) IBOutlet UIButton *creatNewAccount_Bttn;

- (IBAction)keepLoggedInBttnAction:(id)sender;
- (IBAction)logInButtonAction:(id)sender;
- (IBAction)forgotPasswordBttnAction:(id)sender;
- (IBAction)creatAccountBttnAction:(id)sender;

// Forgot Password view

@property (strong, nonatomic) IBOutlet UIView *forgotPasswordSubView;
@property (strong, nonatomic) IBOutlet UIView *forgotPassContentView;
@property (strong, nonatomic) IBOutlet UILabel *enterEmail_Label;
@property (strong, nonatomic) IBOutlet UITextField *emailText_box;
@property (strong, nonatomic) IBOutlet UILabel *forgotPasswordLabel;

- (IBAction)forgotPasswordSubmit_BttnAction:(UIButton *)sender;
- (IBAction)popupDismissalBttnAction:(UIButton *)sender;

@end
