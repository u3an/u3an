//
//  LoginViewController.m
//  U3AN
//
//  Created by Vipin on 03/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "LoginViewController.h"
#import "UserData.h"
#import "SignUpViewController.h"
//#import "Localizations.h"

#define MAXLENGTH 25

BOOL keepMeLoggedIn;
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xibLoading];
    self.name_Label.text = localize(@"Email:");
    self.password_Label.text = localize(@"Password:");
    self.keepLoggedIn_Label.text = localize(@"Keep me logged in");
    self.forgotPasswordLabel.text = localize(@"Forgot Password");
    self.enterEmail_Label.text = localize(@"Enter Email:");
    
    [self.keepLoggedInBttn setBackgroundImage:[UIImage imageNamed:@"tick_box2.png"] forState:UIControlStateNormal];
    [self.keepLoggedInBttn setBackgroundImage:[UIImage imageNamed:@"tick_box1.png"] forState:UIControlStateSelected];
        self.login_ScrollView.contentSize = CGSizeMake(self.login_ScrollView.frame.size.width, self.creatNewAccount_Bttn.frame.origin.y+self.creatNewAccount_Bttn.frame.size.height+100);
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{

    [self updateViewHeading];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateViewHeading
{
    self.login_ScrollView.layer.cornerRadius = 2.0;
    self.login_ScrollView.layer.masksToBounds = YES;
    
    [[HomeTabViewController sharedViewController] resetTabSelection];
    if (ApplicationDelegate.subHomeNav.viewControllers.count==1)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].u3an_InnerPages_Bg.hidden=NO;
    [HomeTabViewController sharedViewController].headerView.hidden=NO;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
    
    [self.forgotPasswordLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:17.0f]];
    [self.enterEmail_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.keepLoggedIn_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.password_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];
    [self.name_Label setFont:[UIFont fontWithName:@"Tahoma" size:15.0f]];

}
- (IBAction)keepLoggedInBttnAction:(id)sender {
    self.keepLoggedInBttn.selected = !self.keepLoggedInBttn.selected;
    if (self.keepLoggedInBttn.selected)
    {
        keepMeLoggedIn=YES;
    }
    else
    {
        keepMeLoggedIn=NO;

    }
    
}

- (IBAction)logInButtonAction:(id)sender {
    [self resignAllResponders];
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        if ((self.passwordTextBox.text.length>0)&&(self.nameTextBox.text.length>0))
        {
            
            [self loginCustomer];
            
        }
        else
        {
            [ApplicationDelegate showAlertWithMessage:kFILL_ALL_FIELDS title:kMESSAGE];
        }
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:kNETWORK_ERROR_MSG title:nil];
    }

}

- (IBAction)forgotPasswordBttnAction:(id)sender {
   
        self.forgotPasswordSubView.frame = CGRectMake(0, 0, ApplicationDelegate.window.rootViewController.view.frame.size.width, ApplicationDelegate.window.rootViewController.view.frame.size.height);
    self.forgotPassContentView.layer.cornerRadius = 2.0;
    self.forgotPassContentView.layer.masksToBounds = YES;
    self.forgotPasswordSubView.layer.cornerRadius = 2.0;
    self.forgotPasswordSubView.layer.masksToBounds = YES;
    
    self.emailText_box.text = @"";
    
    [ApplicationDelegate addViewWithPopUpAnimation:self.forgotPasswordSubView InParentView:ApplicationDelegate.window.rootViewController.view];
}

- (IBAction)creatAccountBttnAction:(id)sender {
    
    SignUpViewController *signUpVc = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    signUpVc.view.backgroundColor = [UIColor clearColor];
    
    if (![ApplicationDelegate.subHomeNav.visibleViewController isKindOfClass:[signUpVc class]])
    {
        [ApplicationDelegate.subHomeNav pushViewController:signUpVc animated:NO];
    }
    
}
-(void)resignAllResponders
{
    [self.nameTextBox resignFirstResponder];
    [self.passwordTextBox resignFirstResponder];
}

-(void)loginCustomer
{
    //NSMutableArray *restaurantListArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *loginData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserSignInPostData]];
    if (loginData.count>0)
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine loginUserWithDataDictionary:loginData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             
             if ([ApplicationDelegate isValid:responseDictionary])
             {
                 if (responseDictionary.count>0)
                 {
                     if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                     {
                         UserData *userItem = [ApplicationDelegate.mapper getUserDataFromDictionary:responseDictionary];
                         
                         if ([userItem.loginStatus isEqualToString:[NSString stringWithFormat: @"success"]])
                         {
                             
                             ApplicationDelegate.logged_User_Name=self.nameTextBox.text;
                             ApplicationDelegate.isLoggedIn=YES;
                             [[HomeTabViewController sharedViewController] updateMoreMenuItems];
                             [ApplicationDelegate showAlertWithMessage:localize(@"Successfully Logged In") title:nil];
                             if (keepMeLoggedIn==YES)
                             {
                                 NSString *plistPath= [ApplicationDelegate fileWithFilename:@"UserDetails.plist"];
                                 NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile: plistPath];
                                 
                                 //add elements to data file and write data to file
                                 
                                 [data setObject:userItem.authKey forKey:@"Authkey"];
                                 [data setObject:userItem.loginStatus forKey:@"Status"];
                                 [data setObject:self.nameTextBox.text forKey:@"UserName"];
                                 
                                 //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
                                 //Default value saving, will update after fetching User Details from server
                                 [data setObject:[NSNumber numberWithBool:NO] forKey:@"newsletterStatus"];
                                 
                                 [data writeToFile: plistPath atomically:YES];
                                 
                             }
                             // Fetches user address for News Letter Status
                             [self getCustomerAddressesAndProceed];
                             
                             
                         }
                         else
                         {
                             [ApplicationDelegate showAlertWithMessage:userItem.loginStatus title:nil];
                         }
//                         if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
//                         {
//                             
//                             [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
//                         }
                     }
                    else
                    {
                        [ApplicationDelegate showAlertWithMessage:[responseDictionary objectForKey:@"Status"] title:nil];
                    }
                 }
             }
             
             [ApplicationDelegate removeProgressHUD];
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
        
    }
    
}

-(NSMutableDictionary *)getUserSignInPostData
{
    
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:self.nameTextBox.text forKey:@"username"];
    [postDic setObject:self.passwordTextBox.text forKey:@"password"];
    
    return postDic;
}

#pragma mark - GET CUSTOMER ADDRESS

-(void)getCustomerAddressesAndProceed
{
    NSMutableDictionary *custPostData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostDataForCustomerInfo]];
    if (custPostData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate.engine getCustomerAddressesWithDataDictionary:custPostData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 ApplicationDelegate.newsLetterStatusFlag = NO;
                 
                 dataFound = YES;
                 
                 NSMutableArray *addressArray = [[NSMutableArray alloc] init];
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     if (responseDictionary.count>0)
                     {
                         if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"Status"]])
                         {
                             
                             if ([[responseDictionary objectForKey:@"Status"] caseInsensitiveCompare:@"success"]==NSOrderedSame)
                             {
                                 
                                 if ([ApplicationDelegate isValid:[responseDictionary objectForKey:@"CustomerDetails"]])
                                 {
                                     addressArray=[[NSMutableArray alloc] initWithArray:[responseDictionary objectForKey:@"CustomerDetails"]];
                                 }
                                 if (addressArray.count>0)
                                 {
                                     if ([ApplicationDelegate isValid:[addressArray objectAtIndex:0]])
                                     {
                                         CustomerAddressDetails *selectedAddressItem = [ApplicationDelegate.mapper getCustAddressDetailsFromDictionary:[addressArray objectAtIndex:0]];
                                         
                                         //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
                                         ApplicationDelegate.newsLetterStatusFlag = selectedAddressItem.subscribed_to_news;
                                         
                                         
                                     }
                                     
                                 }
                                 
                             }
                         }
                     }
                 }
                 //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
                 [ApplicationDelegate saveNewsLetterStatusWithStatus:ApplicationDelegate.newsLetterStatusFlag];
                 if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
                 {
                     
                     [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
                 }
             }
             
             
         } errorHandler:^(NSError *error) {
             
             //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
             ApplicationDelegate.newsLetterStatusFlag = NO;
             
             [ApplicationDelegate saveNewsLetterStatusWithStatus:ApplicationDelegate.newsLetterStatusFlag];
             
             if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
             {
                 
                 [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
             }
         }];
    }
    else
    {
        //********** NEWSLETTER HANDLING (SAVING LOCALLY AND UPDATING SERVER WHEN PERSONAL INFO UPDATES) ***********
        ApplicationDelegate.newsLetterStatusFlag = NO;
        
        [ApplicationDelegate saveNewsLetterStatusWithStatus:ApplicationDelegate.newsLetterStatusFlag];
        
        if (ApplicationDelegate.subHomeNav.viewControllers.count>1)
        {
            
            [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
        }
    }
}

-(NSMutableDictionary *)getUserPostDataForCustomerInfo
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:ApplicationDelegate.logged_User_Name forKey:@"userId"];
    
    
    return postDic;
}

-(void)forgotPassword
{
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    __block BOOL dataFound = NO;
    NSMutableDictionary *restaurantData = [[NSMutableDictionary alloc] initWithDictionary:[self getUserPostData]];
    if (restaurantData.count>0)
    {

    [ApplicationDelegate.engine forgotPassword:restaurantData CompletionHandler:^(NSString *responseString)
     {
         //NSLog(@"%@",responseString);
         
         if (!dataFound)
         {
             dataFound = YES;
             
             [ApplicationDelegate removeProgressHUD];
             
             if (([responseString caseInsensitiveCompare:@"success"]==NSOrderedSame)||([[responseString lowercaseString] containsString:@"success"]))
             {
                 self.emailText_box.text = @"";
                 [self.forgotPasswordSubView removeFromSuperview];
                 [ApplicationDelegate showAlertWithMessage:localize(@"Password has been sent to your email. Please check your mail.") title:@""];
             }
             else
             {
                 [ApplicationDelegate showAlertWithMessage:localize(@"Something went wrong. Please try later.") title:@""];
             }
             
         }
         
     } errorHandler:^(NSError *error) {
   
         
         [ApplicationDelegate removeProgressHUD];
         [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
     }];
    
    }
}

-(NSMutableDictionary *)getUserPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    [postDic setObject:self.emailText_box.text forKey:@"email"];
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    return postDic;
}


#pragma mark - TEXT FIELD DELEGATES

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.emailText_box)
    {
        if (textField.text.length!=0)
        {
            if ( ![ApplicationDelegate isEmailValidWithEmailID:textField.text])
            {
                [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email address") title:kMESSAGE];
                self.emailText_box.text=@"";
            }
        }
        
    }
    

    
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}
#pragma mark - POP UP VIEW ACTIONS


- (IBAction)forgotPasswordSubmit_BttnAction:(UIButton *)sender {
    
    if (self.emailText_box.text.length>0)
    {
        if ( ![ApplicationDelegate isEmailValidWithEmailID:self.emailText_box.text])
        {
            [ApplicationDelegate showAlertWithMessage:localize(@"Please enter a valid email address") title:kMESSAGE];
            self.emailText_box.text=@"";
        }
        else
        {
            [self forgotPassword];
        }
        
    }

}

- (IBAction)popupDismissalBttnAction:(UIButton *)sender {
    
    if ([self.forgotPasswordSubView superview])
    {
        [self.forgotPasswordSubView removeFromSuperview];
    }
}

-(void)xibLoading
{
    NSString *nibName = @"LoginViewController";
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        nibName = [nibName stringByAppendingString:@"_a"];
    }
    
    NSArray *nibObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    UIView *aView = [nibObjs objectAtIndex:0];
    aView.backgroundColor = [UIColor clearColor];
    self.view = aView;
}

@end
