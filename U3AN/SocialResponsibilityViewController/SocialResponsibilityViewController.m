//
//  SocialResponsibilityViewController.m
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SocialResponsibilityViewController.h"

@interface SocialResponsibilityViewController ()

@end

@implementation SocialResponsibilityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self loadPageDetails];
}


-(void)updateViewHeading
{
    self.socialWebPage.layer.cornerRadius = 2.0;
    self.socialWebPage.layer.masksToBounds = YES;
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)loadPageDetails
{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
    if (postData.count>0)
    {
        __block BOOL dataFound = NO;
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        [ApplicationDelegate.engine getPageDetailsWithDataDictionary:postData CompletionHandler:^(NSMutableDictionary *responseDictionary)
         {
             if (!dataFound)
             {
                 dataFound = YES;
                 
                 BOOL success = NO;
                 
                 if ([ApplicationDelegate isValid:responseDictionary])
                 {
                     if (responseDictionary.count>0)
                     {
                         PageDetails *pageItem = [ApplicationDelegate.mapper getPageDetailFromDictionary:responseDictionary];
                         if (pageItem.page_Data.length>0)
                         {
                             success = YES;
                             
                             NSString *widthVal = [NSString stringWithFormat:@"%dpx",((int)(self.socialWebPage.frame.size.width-10))];
                             
                             
                             NSString *formattedHTMLText = [NSString stringWithFormat:@"<html> \n"
                                                            "<head> \n"
                                                            "<style type=\"text/css\"> \n"
                                                            "body {font-family: \"%@\"; font-size: %@;} b {font-weight:bolder; font-size:%@; }\n"
                                                            "</style> \n"
                                                            "</head> \n"
                                                            "<body><div style=\"width: %@; word-wrap: break-word\">%@</div></body> \n"
                                                            "</html>", @"BREESERIF-REGULAR", [NSNumber numberWithInt:11],[NSNumber numberWithInt:11],widthVal, pageItem.page_Data];
                             [self.socialWebPage loadHTMLString:[NSString stringWithFormat:@"<div style=\"width: %@; word-wrap: break-word; color: #ffffff;\">%@</div>",widthVal,formattedHTMLText] baseURL:nil];
                         }
                         
                         
                         // [self.socialWebPage loadHTMLString:pageItem.page_Data baseURL:nil];
                     }
                 }
                 if (!success)
                 {
                     [ApplicationDelegate showAlertWithMessage:kNO_DATA_ERROR_MSG title:nil];
                     
                 }
                 [ApplicationDelegate removeProgressHUD];
             }
             
         } errorHandler:^(NSError *error) {
             
             [ApplicationDelegate removeProgressHUD];
             [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
         }];
    }
    
}



-(NSMutableDictionary *)getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    //[postDic setObject:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    [postDic setObject:@"socialresponsibility" forKey:@"PageName"];
    
    
    return postDic;
}
@end
