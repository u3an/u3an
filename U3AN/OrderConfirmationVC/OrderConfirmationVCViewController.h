//
//  OrderConfirmationVCViewController.h
//  U3AN
//
//  Created by Vipin on 11/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownWithHeaderSelection.h"
#import "CartInformation.h"
#import "PaymentOptionCell.h"
#import "CuisineListItemCell.h"
#import "GeneralRequestCell.h"
@interface OrderConfirmationVCViewController : UIViewController<ListSelectionProtocol,ListWithHeaderSelectionProtocol,paymentOptionCellDelegate,CuisineListItemCellDelegate,UITextFieldDelegate,UIScrollViewDelegate>{
//    MPMoviePlayerController * playerCtrl;
}

//******************************************************************************
//************************** DELIVERY INFORMATION VIEW *************************
//******************************************************************************
@property (strong, nonatomic)NSString * userPointValue;
@property (strong, nonatomic)NSString * userPointMinValue;
@property (strong, nonatomic)NSString * userPointMaxValue;
@property (strong, nonatomic)NSString * userPointInKD;

@property (strong, nonatomic)NSMutableArray *custAddressListArray;
@property (strong, nonatomic)CartInformation *cartInfoItem;

@property (strong, nonatomic) DropDownWithHeaderSelection *areaDropDownObj;
@property (strong, nonatomic) DropDownView *addressDropDownObj;
@property (strong, nonatomic)Areas *areaObj;

@property (strong, nonatomic) IBOutlet UIScrollView *orderConfirmScrollView;

@property (strong, nonatomic) IBOutlet UIView *deliveryInfoView;
@property (strong, nonatomic) IBOutlet UIView *addressWithGoView;
@property (strong, nonatomic) IBOutlet UIView *personalInfoView;
@property (strong, nonatomic) IBOutlet UIView *otherInfoView;

//********* DELIVERY INFO SUB VIEWS **********

@property (strong, nonatomic) IBOutlet UILabel *deliveryHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *villaHouseNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *officeNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *buildingNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *areaNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *areaValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *tipsTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryLocationTextLabel;

@property (strong, nonatomic) IBOutlet UIButton *villaHouseRadioButton;
@property (strong, nonatomic) IBOutlet UIButton *buildingRadioButton;
@property (strong, nonatomic) IBOutlet UIButton *officeRadioButton;

@property (strong, nonatomic) IBOutlet UITextField *areaDropDownValueField;
@property (strong, nonatomic) IBOutlet UIButton *areaDropDownButton;
- (IBAction)areaSelectionBttnAction:(UIButton *)sender;

//******************************

//********* ADDRESS & ADD NEW ADDRESS - SUB VIEWS **********

@property (strong, nonatomic) IBOutlet UILabel *adressNameLabel;
@property (strong, nonatomic) IBOutlet UITextField *adressDropDownValueField;
@property (strong, nonatomic) IBOutlet UIButton *adressDropDownButton;
@property (strong, nonatomic) IBOutlet UIButton *addnewAddressButton;
- (IBAction)selectAddressButtonAction:(UIButton *)sender;

- (IBAction)AddAddressBttnAction:(UIButton *)sender;

//******************************

//********* PERSONAL INFO - SUB VIEWS **********

@property (strong, nonatomic) IBOutlet UILabel *firstNameTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastNameTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *mobileTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailTextLabel;

@property (strong, nonatomic) IBOutlet UITextField *firstNameValueField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameValueField;
@property (strong, nonatomic) IBOutlet UITextField *mobileValueField;
@property (strong, nonatomic) IBOutlet UITextField *emailValueField;

//******************************

//********* OTHER INFO - SUB VIEWS **********

@property (strong, nonatomic) IBOutlet UILabel *blockNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *streetNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *juddaNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *buildingNumberNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *floorNumberNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *apartmentNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *extraDirNameLabel;

@property (strong, nonatomic) IBOutlet UITextField *blockValueField;
@property (strong, nonatomic) IBOutlet UITextField *streetValueField;
@property (strong, nonatomic) IBOutlet UITextField *juddaValueField;
@property (strong, nonatomic) IBOutlet UITextField *buildingNumValueField;
@property (strong, nonatomic) IBOutlet UITextField *floorNumValueField;
@property (strong, nonatomic) IBOutlet UITextField *apartmentValueField;
@property (strong, nonatomic) IBOutlet UITextView *extraDirValueField;

@property (strong, nonatomic) IBOutlet UIView *villaOptionalView;
@property (strong, nonatomic) IBOutlet UIView *extraDirView;


//*********************

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *saveAndContinueButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;

- (IBAction)tapOutsideTextFieldAction:(id)sender;

- (IBAction)deliveryInfoOptionSelection:(UIButton *)sender;

- (IBAction)saveAndContinueAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)nextButtonAction:(UIButton *)sender;

//******************************************************************************
//*************************** ORDER SUMMARY VIEW **************************
//******************************************************************************



@property (strong, nonatomic) IBOutlet UIView *orderSummaryView;
@property (strong, nonatomic) IBOutlet UILabel *orderSummaryHeaderLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *orderSummaryScroll;

@property (strong, nonatomic) DropDownView *orderSummaryDeliveryAtDropDownObj;

@property (strong, nonatomic) IBOutlet UIButton *orderSummaryBackButton;
@property (strong, nonatomic) IBOutlet UIButton *orderSummaryNextButton;

- (IBAction)orderSummaryBackButtonAction:(id)sender;

- (IBAction)orderSummaryNextButtonAction:(id)sender;

//******************************************************************************
//*************************** CONFIRM YOUR ORDER VIEW **************************
//******************************************************************************


@property (strong, nonatomic) IBOutlet UIView *confirmYourOrderView;
@property (strong, nonatomic) IBOutlet UILabel *confirmYourOrderHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *confirmOrderPaymentsLabel;

@property (strong, nonatomic) IBOutlet UIImageView *cashRadioOptionImage;
@property (strong, nonatomic) IBOutlet UIImageView *knetRadioOptionImage;
@property (strong, nonatomic) IBOutlet UIImageView *visaRadioOptionImage;
@property (strong, nonatomic) IBOutlet UIImageView *u3anCreditRadioOptionImage;

@property (strong, nonatomic) IBOutlet UIButton *cashRadioOptionButton;
@property (strong, nonatomic) IBOutlet UIButton *knetRadioOptionButton;
@property (strong, nonatomic) IBOutlet UIButton *visaRadioOptionButton;
@property (strong, nonatomic) IBOutlet UIButton *u3anCreditOptionButton;

@property (strong, nonatomic) IBOutlet UIScrollView *confirmYourOrderScroll;
@property (strong, nonatomic) IBOutlet UIView *confirmOrderDetailsContainerView;

@property (strong, nonatomic) IBOutlet UILabel *subTotalTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryChargesTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *grandTotalTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *payByCashTextLabel;

@property (strong, nonatomic) IBOutlet UILabel *subTotalValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryChargesValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *grandTotalValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *payByCashValueLabel;

@property (strong, nonatomic) IBOutlet UIButton *confirmOrderButton;
- (IBAction)confirmOrderBackButtonAction:(id)sender;

- (IBAction)paymentRadioOptionSelection:(UIButton *)sender;

- (IBAction)confirmOrderButtonAction:(id)sender;
- (IBAction)confirmOrderButtonAnimationAction:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *orderSumView;
@property (strong, nonatomic) IBOutlet UIView *confOrderVew;
- (IBAction)textFieldkeyHide:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UITextField *pointTxtField;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *myPointsTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *myPointsValueLabel;

@property (weak, nonatomic) IBOutlet UIButton *useFullPointsRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *myPointsRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *notRequiredRadioBtn;

@property (weak, nonatomic) IBOutlet UILabel *useFullPointRadioBtnLbl;
@property (weak, nonatomic) IBOutlet UILabel *myPointsRadioBtnLbl;
@property (weak, nonatomic) IBOutlet UILabel *notRequiredRadioBtnLbl;

@property (weak, nonatomic) IBOutlet UILabel *pointsRequiredForThisOrder;
@property (weak, nonatomic) IBOutlet UILabel *pointsInKdLbl;
- (IBAction)pointsRadioBtnActn:(UIButton *)sender;
- (IBAction)textFieldValueChanged:(UITextField *)sender;

- (IBAction)textFieldEditingChanged:(UITextField *)sender;
@property (weak, nonatomic) IBOutlet UILabel *u3anPointsLbl;

@property (weak, nonatomic) IBOutlet UIView *subTotalBackView;
@property (weak, nonatomic) IBOutlet UIView *deliveryChargesBackView;
@property (weak, nonatomic) IBOutlet UIView *discountBackView;
@property (weak, nonatomic) IBOutlet UIView *usedUanPointsBackView;
@property (weak, nonatomic) IBOutlet UIView *grandTotalBackView;
@property (weak, nonatomic) IBOutlet UIView *payByCashBackView;
@property (weak, nonatomic) IBOutlet UIImageView *firstSeperator;
@property (weak, nonatomic) IBOutlet UIImageView *secondSeperator;
@property (weak, nonatomic) IBOutlet UIView *pointTxtBackView;
@property (weak, nonatomic) IBOutlet UIView *confOrderBackView;
@property (weak, nonatomic) IBOutlet UIView *u3anPointLblBackView;
@property (weak, nonatomic) IBOutlet UIView *confirmYourOrderInnerContainer;
@property (weak, nonatomic) IBOutlet UILabel *u3anPointsDescriptionLabel;


@end
