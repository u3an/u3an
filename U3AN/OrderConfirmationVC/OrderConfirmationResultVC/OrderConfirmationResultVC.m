//
//  OrderConfirmationResultVC.m
//  U3AN
//
//  Created by Vipin on 10/04/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "OrderConfirmationResultVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>

@interface OrderConfirmationResultVC ()
{
    BOOL status;
}

@end

@implementation OrderConfirmationResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    webviewOriginalFrame = self.resultWebView.frame;

    status=YES;

    animationFlag = 0;
    
    self.animationBackView.layer.cornerRadius = 3.0;
    self.animationBackView.layer.masksToBounds = YES;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self playAnimation];
    [HomeTabViewController sharedViewController].homeBtn.hidden = NO;
    [HomeTabViewController sharedViewController].chatBtn.hidden = NO;
    webviewOriginalFrame = self.resultWebView.frame;
    [self updateViewHeading];
    [self setupUI];

}

- (void)playAnimation{

    self.animationBackView.hidden=NO;
    
  //  NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"sounded_ready_order_anim" ofType:@"mp4"];

    self.animationBackView.hidden = NO;
//    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"sounded_ready_order_anim" ofType:@"mp4"];
    
    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"sounded_ready_order_gray_bg" ofType:@"mp4"];
    
//NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"New" ofType:@"mp4"];

    NSURL* movieURL = [NSURL fileURLWithPath:moviePath];
    
    playerCtrl =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    playerCtrl.scalingMode = MPMovieScalingModeAspectFit;
    playerCtrl.controlStyle = MPMovieControlStyleNone;
    playerCtrl.repeatMode = MPMovieRepeatModeOne;
    playerCtrl.backgroundView.backgroundColor = [UIColor clearColor];
    playerCtrl.view.backgroundColor = [UIColor clearColor];
    for(UIView* subV in playerCtrl.view.subviews) {
        subV.backgroundColor = [UIColor clearColor];
    }
    
//    playerCtrl.view.frame = [[UIScreen mainScreen]bounds];
    CGRect frame = self.animationBackView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    playerCtrl.view.frame = frame;
    //playerCtrl.view.frame = CGRectMake(0, 0, 50, 50);
    
    [playerCtrl setScalingMode:MPMovieScalingModeFill];
    splashDefault = [NSUserDefaults standardUserDefaults];
    [splashDefault setObject:@"2" forKey:@"SplashFlag"];
    [splashDefault synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish2)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    //[playerCtrl.view setCenter:CGPointMake(240, 160)];
    //[playerCtrl.view setFrame:CGRectMake(0, 0, 480, 320)];
    //[playerCtrl.view setTransform:CGAffineTransformMakeRotation(-M_PI/2)];
    [self.animationBackView addSubview:playerCtrl.view];
    [playerCtrl play];
}

-(void) moviePlayBackDidFinish2{
    NSLog(@"Finished");
    //[self.animationBackView removeFromSuperview];
    
    //[self updateViewHeading];
    //[self setupUI];
}

-(void)updateViewHeading
{
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
  //  [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)setupUI
{
    self.resultWebView.layer.cornerRadius = 3.0;
    self.resultWebView.layer.masksToBounds = YES;
    self.resultWebView.scalesPageToFit = YES;
    [self.resultWebView.scrollView setContentSize: CGSizeMake(self.resultWebView.frame.size.width, self.resultWebView.scrollView.contentSize.height)];
//    [ApplicationDelegate addProgressHUDToView:self.resultWebView];
    [ApplicationDelegate addProgressHUDToView:self.view];
    [self.resultWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    
}

#pragma mark - UIWebViewDelegate Protocol Methods

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
//    [ApplicationDelegate addProgressHUDToView:self.resultWebView];
    
    NSString *CurrentUrlString = webView.request.URL.absoluteString;
    
    NSLog(@"%@",CurrentUrlString);
    
    NSString *substring = @"http://www.u3an.com/Receipt.aspx";
    
    if ([CurrentUrlString localizedCaseInsensitiveContainsString:substring]) {
        self.resultWebView.frame = webviewOriginalFrame;
        [self.resultWebView.scrollView setContentSize: CGSizeMake(self.resultWebView.frame.size.width, self.resultWebView.scrollView.contentSize.height)];
        [playerCtrl stop];
    }
    
    [ApplicationDelegate addProgressHUDToView:self.view];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    
    self.resultWebView.userInteractionEnabled = YES;
    
    NSString *CurrentUrlString = webView.request.URL.absoluteString;
    
    NSLog(@"%@",CurrentUrlString);
    
    NSString *substring = @"http://www.u3an.com/Thankyou.aspx";
    
    [ApplicationDelegate removeProgressHUD];

    
    
    if ([CurrentUrlString localizedCaseInsensitiveContainsString:substring]) {
        if (status){
            
            self.resultWebView.frame = CGRectMake(webviewOriginalFrame.origin.x, webviewOriginalFrame.origin.x + (self.animationBackView.frame.origin.y + self.animationBackView.frame.size.height + 8), webviewOriginalFrame.size.width, webviewOriginalFrame.size.height - (self.animationBackView.frame.origin.y + self.animationBackView.frame.size.height));
            [self playAnimation];
            NSLog(@"%f",webviewOriginalFrame.size.height);
            NSLog(@"%f",(self.animationBackView.frame.origin.y + self.animationBackView.frame.size.height));
            NSLog(@"%f",self.resultWebView.frame.origin.x);
            NSLog(@"%f",self.resultWebView.frame.origin.y);
            NSLog(@"%f",self.resultWebView.frame.size.width);
            NSLog(@"%f",self.resultWebView.frame.size.height);
            status=NO;
        }
    }
    

    NSLog(@"%@",webView.request.URL.absoluteString);
 //   NSString *CurrentUrlString = webView.request.URL.absoluteString;
   // NSString *substring = @"http://www.u3an.com/Thankyou.aspx";
    
    if ([CurrentUrlString localizedCaseInsensitiveContainsString:substring]) {
        if (animationFlag == 0) {
            animationFlag = 1;
            self.resultWebView.frame = CGRectMake(webviewOriginalFrame.origin.x, webviewOriginalFrame.origin.x + (self.animationBackView.frame.origin.y + self.animationBackView.frame.size.height + 8), webviewOriginalFrame.size.width, webviewOriginalFrame.size.height - (self.animationBackView.frame.origin.y + self.animationBackView.frame.size.height));
            NSLog(@"%f",self.view.frame.size.height);
            [self playAnimation];
        }
    }
    NSLog(@"%f",self.resultWebView.frame.size.height);
}

- (void) viewWillDisappear:(BOOL)animated{
    [HomeTabViewController sharedViewController].homeBtn.hidden = YES;
    [HomeTabViewController sharedViewController].chatBtn.hidden = YES;
    [playerCtrl stop];
}

@end
