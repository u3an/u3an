//
//  CuisineListItemCell.m
//  U3AN
//
//  Created by Vipin on 16/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "CuisineListItemCell.h"

@implementation CuisineListItemCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"CuisineListItemCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (CuisineListItemCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"CuisineListItemCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (CuisineListItemCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}
- (IBAction)removeBttnAction:(UIButton *)sender
{
    [self.delegateCuisineItemCell removeCuisineItemButtonDidClicked:self];
}
@end
