//
//  PaymentOptionCell.h
//  U3AN
//
//  Created by Vipin on 16/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol paymentOptionCellDelegate;

@interface PaymentOptionCell : UIView

@property(weak,nonatomic)id<paymentOptionCellDelegate> delegatepaymentOptionCell;

@property (strong, nonatomic)RestaurantCart *restaurantCartObject;

@property (strong, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (strong, nonatomic) IBOutlet UILabel *restaurant_NameLbl;
@property (strong, nonatomic) IBOutlet UILabel *paymentOptionsNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryNowNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryAtNameLabel;

@property (strong, nonatomic) IBOutlet UIButton *deliverNowButton;
@property (strong, nonatomic) IBOutlet UIButton *deliverAtButton;
@property (strong, nonatomic) IBOutlet UITextField *deliverAtTxtValue;
@property (strong, nonatomic) IBOutlet UIImageView *visaImgView;
@property (strong, nonatomic) IBOutlet UIImageView *creditImgView;
@property (strong, nonatomic) IBOutlet UIImageView *kNetImgView;
@property (strong, nonatomic) IBOutlet UIImageView *cashImgView;
@property (strong, nonatomic) IBOutlet UILabel *menuLbl;
@property (strong, nonatomic) IBOutlet UILabel *itemNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UIButton *deliveryAtDropDownButton;

@end
@protocol paymentOptionCellDelegate <NSObject>
- (void) deliveryTimeRadioBttnDidClicked:(PaymentOptionCell *) paymentOptionCellItem;
- (void) deliveryAtTimeDropDownBttnDidClicked:(PaymentOptionCell *) paymentOptionCellItem;
@end