//
//  GeneralRequestCell.h
//  U3AN
//
//  Created by Ratheesh on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralRequestCell : UIView

@property (strong, nonatomic) IBOutlet UILabel *subTotalNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryChargesNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantTotalNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *subTotalValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryChargesValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *discountValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *restaurantTotalValueLabel;

@property (strong, nonatomic) IBOutlet UILabel *generalRequestNameLabel;

@property (strong, nonatomic) IBOutlet UITextField *generalRequestTxtField;
@end
