//
//  GeneralRequestCell.m
//  U3AN
//
//  Created by Ratheesh on 25/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "GeneralRequestCell.h"

@implementation GeneralRequestCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"GeneralRequestCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (GeneralRequestCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"GeneralRequestCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (GeneralRequestCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}
@end
