//
//  LiveChatViewController.m
//  U3AN
//
//  Created by satheesh on 4/1/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "LiveChatViewController.h"
#import "HomeTabViewController.h"

@interface LiveChatViewController ()

@end

@implementation LiveChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self updateViewHeading];
    [self loadPageDetails];
    
}


-(void)updateViewHeading
{
    self.liveChatWeb.layer.cornerRadius = 3.0;
    self.liveChatWeb.layer.masksToBounds = YES;
    [[HomeTabViewController sharedViewController] resetTabSelection];
    [HomeTabViewController sharedViewController].moreButton.selected=YES;
    if (ApplicationDelegate.subHomeNav.viewControllers.count==2)
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden=YES;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=YES;
    }
    else
    {
        [HomeTabViewController sharedViewController].navigationBackButton.hidden= NO;
        [HomeTabViewController sharedViewController].navigationBackImage.hidden=NO;
        
    }
    [HomeTabViewController sharedViewController].headingLabel.text = [ApplicationDelegate getViewTitle:self];
    [HomeTabViewController sharedViewController].mostSellingHeaderButtonView.hidden=YES;
    [HomeTabViewController sharedViewController].searchFilterButton.hidden=YES;
    [HomeTabViewController sharedViewController].cartControlView.hidden = YES;
}

-(void)loadPageDetails
{
  
    [ApplicationDelegate addProgressHUDToView:self.view];
    NSURL * url = [NSURL URLWithString:LIVE_CHAT_URL];
     [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
     NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.liveChatWeb loadRequest:requestObj];
    [ApplicationDelegate removeProgressHUD];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

}

#pragma mark - UIWebView Delegates

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    NSError *error = nil;
    NSString *jsFromFile = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]
                                                              URLForResource:@"liveChatJS" withExtension:@"html"]
                                                    encoding:NSUTF8StringEncoding error:&error];
    
    __unused NSString *jsOverrides = [webView stringByEvaluatingJavaScriptFromString:jsFromFile];
    
    /*
    NSString *expr = @"document.getElementById('btnClose').value";
    
    //NSLog(@"RESUL>>>%@",[webView stringByEvaluatingJavaScriptFromString:expr]);
    
    // this is a pop-up window
    
    if (self.liveChatWeb==webView)
    {
        // overwrite the 'window.close' to be a 'back://' URL
        
        NSError *error = nil;
        NSString *jsFromFile = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]
                                                                  URLForResource:@"liveChatJS" withExtension:@"html"]
                                                        encoding:NSUTF8StringEncoding error:&error];
        
        __unused NSString *jsOverrides = [webView
                                          stringByEvaluatingJavaScriptFromString:jsFromFile];
    }
     */
}


- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType;
{
//    NSLog(@"request: %@", request.URL.relativePath);
//    NSString *url = request.URL.absoluteString;
//    NSLog(@"url: %@", request.URL.scheme);
    
    
//    if ([request.URL.scheme isEqualToString:@"inapp"]) {
//        if ([request.URL.host isEqualToString:@"capture"]) {
//            [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
//        }
//        return NO;
//    }
    
    
    if ([request.URL.scheme isEqualToString:@"back"])
    {
        [ApplicationDelegate.subHomeNav popViewControllerAnimated:NO];
        return NO;
    }
    
    return YES;
}

@end
