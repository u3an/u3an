//
//  PromotionCell.m
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PromotionCell.h"

@implementation PromotionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"PromotionCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (PromotionCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}

-(IBAction)promotionDetailButtonAction:(UIButton *)sender
{
    [self.delegatePromotionItem promotionItemDidClicked:self];
}


@end
