//
//  PromotionDetailVC.h
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionDetailCell.h"
#import "AllRestaurantPromotion.h"
#import "Restaurant.h"

@interface PromotionDetailVC : UIViewController<promotionDetailItemDelegate>

//@property(strong,nonatomic)AllRestaurantPromotion *allPromoRestaurantObj;
@property(strong,nonatomic)Restaurant *promoRestaurantObj;
@property (strong, nonatomic) IBOutlet UIScrollView *promotionDetailScrollView;
@property (strong, nonatomic) IBOutlet UIView *promotionalItemSubView;
@property (strong, nonatomic) IBOutlet UITextView *restaurantDescriptionTxt;
@property (strong, nonatomic) IBOutlet UILabel *restaurantNameLbl;

@end
