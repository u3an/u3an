//
//  PromotionDetailCell.m
//  U3AN
//
//  Created by Vipin on 17/03/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "PromotionDetailCell.h"

@implementation PromotionDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *nib = @"PromotionDetailCell";
        NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
        self = (PromotionDetailCell*)[nibViews objectAtIndex: 0];
    }
    return self;
}
-(IBAction)promotionDetailButtonAction:(UIButton *)sender
{
    [self.delegatePromotionDetailItem promotionDetailItemDidClicked:self];
}


@end
