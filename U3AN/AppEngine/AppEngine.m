//
//  AppEngine.m
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "AppEngine.h"
#import "SBJson.h"

@implementation AppEngine
- (UIImage *)getImageFromURL:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLCacheStorageAllowed timeoutInterval:20.0];
    //    request = [NSURLRequest requestWithURL:url cachePolicy:NSURLCacheStorageAllowed timeoutInterval:20.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *result = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&response error:&error];
    if (error!=nil) {
        return nil;
    }
    UIImage *resultImage = [UIImage imageWithData:(NSData *)result];
    
    // NSLog(@"urlString: %@",urlString);
    return resultImage;
}

-(void)getImageFromServerASYNCWithUrl:(NSString *)url completionHandler:(ResponseImage)response errorHandler:(MKNKErrorBlock)errorBlock
{
    if (url.length>0)
    {
        self.imageOperation = [self imageAtURL:[NSURL URLWithString:url] completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache)
                               {
                                   response(fetchedImage);
                                   
                               } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
                                   
                                   errorBlock(error);
                               }];
    }
    else
    {
        //response(kNO_IMAGE);
    }
    
    
}

-(void)sendPushNotificationDeviceTokenDataWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kAdd_DeviceToken_URL params:postDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getBannerDetailsFromServerWithCompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    //[postDic setValue:kLocal forKey:@"locale"];
    if (![ApplicationDelegate.language isEqualToString:kENGLISH])
    {
        [postDic setObject:k_AR_Local forKey:@"locale"];
    }
    else
    {
        [postDic setObject:kLocal forKey:@"locale"];
    }
    
    MKNetworkOperation *op = [self operationWithPath:kGet_BannerDetails params:postDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getRestaurantsByCountryWithDataDictionary:(NSMutableDictionary *)resDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_ReastaurantsByCountry_URL params:resDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantList"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getMostSellingDishesWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_MostSelling_Dishes_URL params:mDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"DishList"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getMostSellingRestaurantWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_MostSelling_Restaurant_URL params:mDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getCuisinesWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Cuisines_URL params:mDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"cuisineDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getAreasWithDataDictionary:(NSMutableDictionary *)areaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Areas_URL params:areaDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"AllAreaDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getAreasByRestaurantWithDataDictionary:(NSMutableDictionary *)areaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_AreasByRestaurant_URL params:areaDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"AreaDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getRestaurantDetailWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantDetails_URL params:restaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getRestaurantByAreaWithDataDictionary:(NSMutableDictionary *)restaurantAreaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantsByArea_URL params:restaurantAreaDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getNewRestaurantListWithDataDictionary:(NSMutableDictionary *)newRestaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_NewRestaurantList_URL params:newRestaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getRestaurantMenuSectionsWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantMenuSections_URL params:restaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"MenuSectionList"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getMostSellingDishesByRestaurantListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_MostSelling_DishesByRestaurant_URL params:restaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"DishList"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)getMostSellingDishesByCuisineListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_MostSelling_DishesByCuisine_URL params:restaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"DishList"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)getRestaurantByPromotionsListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Restaurant_ByPromotions_URL params:restaurantDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"PromotionItemsDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)getRestaurantListByAdvancedSearchWithDataDictionary:(NSMutableDictionary *)searchDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantsAdvancedSearch_URL params:searchDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)loginUserWithDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kLoginCustomer_URL params:logDic httpMethod:@"POST"];
    
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"LoginResult"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)registerUserWithDataDictionary:(NSMutableDictionary *)registerDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kRegisterCustomer_URL params:registerDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        NSLog(@"%@",[postDataDict jsonEncodedKeyValueString]);
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)addNewRestaurantWithDataDictionary:(NSMutableDictionary *)addNewDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAdd_NewRestaurant_URL params:addNewDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}


-(void)getDishesMenuDetailsWithDataDictionary:(NSMutableDictionary *)dishDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantMenuItemDetails_URL params:dishDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"MenuDetails"]);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getOrderCountCompletionHandler :(ResponseStringBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_OrderCount_URL params:Nil httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        
        responce([completedOperation responseString]);

        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];

}

-(void)forgotPassword:(NSMutableDictionary *)dataDic CompletionHandler :(ResponseStringBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kForgotPassword_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        
        responce([completedOperation responseString]);
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
    
}

-(void)getRestaurantReviewWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_RestaurantReview_URL params:restaurantDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"review"]);
           
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)addToCartWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAdd_To_Cart_URL params:restaurantDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)addToTempCartWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAdd_To_Temp_Cart_URL params:restaurantDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)removeFromCartWithDataDictionary:(NSMutableDictionary *)cartDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:Remove_Cart_Item_URL params:cartDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)removeFromTempCartWithDataDictionary:(NSMutableDictionary *)cartDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:Remove_TempCart_Item_URL params:cartDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)getCartInfoWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_CartInfo_URL params:userDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getTempCartInfoWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Temp_CartInfo_URL params:userDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getCartCountWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_CartCount_URL params:userDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}
-(void)getTempCartCountWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Temp_CartCount_URL params:userDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getPageDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Page_Details_URL params:pageDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getAllRestaurantPromotionsWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_AllRestaurant_Promotions_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"RestaurantDetailsWithItem"] );
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)getRestaurantsByPromotionsWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Restaurant_By_Promotions_URL params:restaurantDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)postFeedbackWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kPost_Feedback_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}
-(void)getGiftVouchersWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Gift_Voucher_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"GiftVouchersList"] );
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)buyGiftVoucherWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kBuy_Gift_Voucher_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getU3ANCreditIntervalsWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_U3AN_Credit_Intervals_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"Interval"] );
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)buyU3ANCreditWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kBuy_U3AN_Credit_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getCustomerAddressesWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Customer_Addresses_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)getFavouriteWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kGet_Favourite_URL params:dataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject[@"FavouritesList"] );
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    
    [self enqueueOperation:op];
}

-(void)editCustomerDetailsWithDataDictionary:(NSMutableDictionary *)custDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kEditCustomer_Details_URL params:custDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)addAddressDetailsWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAdd_Address_Details_URL params:addressDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)changePasswordWithDataDictionary:(NSMutableDictionary *)userDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kChange_Password_URL params:userDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)removeAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kRemove_Address_URL params:addressDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)makeAsPrimaryAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kMake_As_PrimaryAddress_URL params:addressDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)editAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kEdit_Address_URL params:addressDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}
-(void)addToFavouriteWithDataDictionary:(NSMutableDictionary *)favDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kAdd_TO_Favourite_URL params:favDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)removeFromFavouriteWithDataDictionary:(NSMutableDictionary *)favDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kRemove_From_Favourite_URL params:favDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)sendPlaceOrderRequestWithPostData:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op;
    
    if (ApplicationDelegate.isLoggedIn)
    {
        op = [self operationWithPath:kPlaceOrder_Request_URL params:dataDic httpMethod:@"POST"];
    }
    else
    {
        op = [self operationWithPath:kPlaceQuickOrder_Request_URL params:dataDic httpMethod:@"POST"];
    }
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}
-(void)OrderDetailsWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kOrder_Details_URL params:addressDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}


-(void)postItemRatingWithDataDictionary:(NSMutableDictionary *)ratingDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:kPost_ItemRating_URL params:ratingDataDic httpMethod:@"POST"];
    
    //////////
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    // [op postDataEncoding];
    //
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    ////////
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void)showErrorBlockAlert
{
    if (![ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate showAlertWithMessage:kNETWORK_ERROR_MSG title:nil];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:kSERVER_ERROR_MSG title:nil];
    }
}
@end
