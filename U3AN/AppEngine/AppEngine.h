//
//  AppEngine.h
//  U3AN
//
//  Created by Vipin on 12/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//


#import "MKNetworkEngine.h"

@interface AppEngine : MKNetworkEngine

typedef void (^ResponseStringBlock)(NSString* responseString);
typedef void (^ResponseDictionaryBlock)(NSMutableDictionary* responseDictionary);
typedef void (^ResponseArrayBlock)(NSMutableArray* responseArray);
typedef void (^ResponseImage)(UIImage* responseImage);

@property(nonatomic,retain)MKNetworkOperation *imageOperation;

- (UIImage *)getImageFromURL:(NSString *)urlString;

-(void)getImageFromServerASYNCWithUrl:(NSString *)url completionHandler:(ResponseImage)response errorHandler:(MKNKErrorBlock)errorBlock;

-(void)sendPushNotificationDeviceTokenDataWithDataDictionary:(NSMutableDictionary *)postDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getBannerDetailsFromServerWithCompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

-(void)getRestaurantsByCountryWithDataDictionary:(NSMutableDictionary *)resDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getMostSellingDishesWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getMostSellingRestaurantWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getCuisinesWithDataDictionary:(NSMutableDictionary *)mDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getAreasWithDataDictionary:(NSMutableDictionary *)areaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getAreasByRestaurantWithDataDictionary:(NSMutableDictionary *)areaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantDetailWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantByAreaWithDataDictionary:(NSMutableDictionary *)restaurantAreaDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getNewRestaurantListWithDataDictionary:(NSMutableDictionary *)newRestaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantMenuSectionsWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getMostSellingDishesByRestaurantListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getMostSellingDishesByCuisineListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantByPromotionsListWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantListByAdvancedSearchWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)loginUserWithDataDictionary:(NSMutableDictionary *)logDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)registerUserWithDataDictionary:(NSMutableDictionary *)registerDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)addNewRestaurantWithDataDictionary:(NSMutableDictionary *)addNewDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getDishesMenuDetailsWithDataDictionary:(NSMutableDictionary *)dishDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getOrderCountCompletionHandler :(ResponseStringBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)forgotPassword:(NSMutableDictionary *)dataDic CompletionHandler :(ResponseStringBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantReviewWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)addToCartWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)addToTempCartWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)removeFromCartWithDataDictionary:(NSMutableDictionary *)cartDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)removeFromTempCartWithDataDictionary:(NSMutableDictionary *)cartDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getCartInfoWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getTempCartInfoWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getCartCountWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getTempCartCountWithDataDictionary:(NSMutableDictionary *)userDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getPageDetailsWithDataDictionary:(NSMutableDictionary *)pageDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getAllRestaurantPromotionsWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getRestaurantsByPromotionsWithDataDictionary:(NSMutableDictionary *)restaurantDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)postFeedbackWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getGiftVouchersWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)buyGiftVoucherWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getU3ANCreditIntervalsWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)buyU3ANCreditWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getCustomerAddressesWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)getFavouriteWithDataDictionary:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseArrayBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)editCustomerDetailsWithDataDictionary:(NSMutableDictionary *)custDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)addAddressDetailsWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)changePasswordWithDataDictionary:(NSMutableDictionary *)userDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)removeAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)makeAsPrimaryAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)editAddressWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)addToFavouriteWithDataDictionary:(NSMutableDictionary *)favDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)removeFromFavouriteWithDataDictionary:(NSMutableDictionary *)favDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)sendPlaceOrderRequestWithPostData:(NSMutableDictionary *)dataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)OrderDetailsWithDataDictionary:(NSMutableDictionary *)addressDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
-(void)postItemRatingWithDataDictionary:(NSMutableDictionary *)ratingDataDic CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;
@end
