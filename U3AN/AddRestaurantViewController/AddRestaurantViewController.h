//
//  AddRestaurantViewController.h
//  U3AN
//
//  Created by Vipin on 13/01/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "Cuisine.h"

@interface AddRestaurantViewController : UIViewController<ListSelectionProtocol>

@property (strong, nonatomic) DropDownView *cuisineDropDownObj;
@property (strong, nonatomic)Cuisine *cuisineObj;

@property (weak, nonatomic) IBOutlet UILabel *add_Your_Restaurant_Label;
@property (weak, nonatomic) IBOutlet UITextField *first_Name_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *last_Name_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *restaurant_Name_TxtBox;
@property (weak, nonatomic) IBOutlet UITextView *restaurant_Address_TxtView;
@property (weak, nonatomic) IBOutlet UITextField *telephone_TxtBox;
@property (weak, nonatomic) IBOutlet UITextField *email_TxtBox;
@property (weak, nonatomic) IBOutlet UIButton *have_Delivery;
@property (weak, nonatomic) IBOutlet UILabel *cuisines_LabelTxt;
@property (weak, nonatomic) IBOutlet UIButton *cuisine_selection_Bttn;
@property (weak, nonatomic) IBOutlet UIScrollView *add_Restaurant_ScrollView;
@property (weak, nonatomic) IBOutlet UIButton *submit_Button;

// Label Delegates

@property (weak, nonatomic) IBOutlet UILabel *intro_Label;
@property (weak, nonatomic) IBOutlet UILabel *first_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *last_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_Name_Label;
@property (weak, nonatomic) IBOutlet UILabel *restaurant_Address_Label;
@property (weak, nonatomic) IBOutlet UILabel *telephone_Label;
@property (weak, nonatomic) IBOutlet UILabel *email_Label;
@property (weak, nonatomic) IBOutlet UILabel *have_Delivery_Label;
@property (weak, nonatomic) IBOutlet UILabel *cuisine_Label;

- (IBAction)backgroundTapAction:(id)sender;

- (IBAction)have_Delivery_BttnAction:(UIButton *)sender;
- (IBAction)cuisine_SelectionBttnAction:(UIButton *)sender;
- (IBAction)submit_BttnAction:(UIButton *)sender;

@end
