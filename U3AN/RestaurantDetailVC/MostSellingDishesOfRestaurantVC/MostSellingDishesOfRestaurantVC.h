//
//  MostSellingDishesOfRestaurantVC.h
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantDetail.h"
#import "MostSellingDishOfRestaurantCell.h"
#import "RestaurantMenuListVC.h"

@interface MostSellingDishesOfRestaurantVC : UIViewController<mostSellingDishesItemDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mostSellingDishLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mostSellingDishesCellScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *mostSellingDishesMainScrollView;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;

@end
