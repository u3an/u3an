//
//  MostSellingDishOfRestaurantCell.h
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol mostSellingDishesItemDelegate;
@interface MostSellingDishOfRestaurantCell : UIView
@property(weak,nonatomic)id<mostSellingDishesItemDelegate> delegateMostSellingDishesItem;
@property (weak, nonatomic) IBOutlet UIImageView *dishesImageView;
@property (weak, nonatomic) IBOutlet UILabel *dishNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishDescriptionLabel;

@end
@protocol mostSellingDishesItemDelegate <NSObject>
- (void) dishesItemDidClicked:(MostSellingDishOfRestaurantCell *) dishesCategoryItem;
@end