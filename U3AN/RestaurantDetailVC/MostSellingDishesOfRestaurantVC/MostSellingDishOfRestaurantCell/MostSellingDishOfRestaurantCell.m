//
//  MostSellingDishOfRestaurantCell.m
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "MostSellingDishOfRestaurantCell.h"

@implementation MostSellingDishOfRestaurantCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"MostSellingDishOfRestaurantCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MostSellingDishOfRestaurantCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"MostSellingDishOfRestaurantCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (MostSellingDishOfRestaurantCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}

-(IBAction)moreButtonAction:(UIButton *)sender
{
    [self.delegateMostSellingDishesItem dishesItemDidClicked:self];
}

@end
