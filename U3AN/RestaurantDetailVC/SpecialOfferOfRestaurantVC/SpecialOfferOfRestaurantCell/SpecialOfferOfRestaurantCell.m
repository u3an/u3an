//
//  SpecialOfferOfRestaurantCell.m
//  U3AN
//
//  Created by Vipin on 05/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "SpecialOfferOfRestaurantCell.h"

@implementation SpecialOfferOfRestaurantCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (![ApplicationDelegate.language isEqualToString:kENGLISH])
        {
            NSString *nib = @"SpecialOfferOfRestaurantCell_a";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (SpecialOfferOfRestaurantCell*)[nibViews objectAtIndex: 0];
        }
        else{
            NSString *nib = @"SpecialOfferOfRestaurantCell";
            NSArray * nibViews =   [[NSBundle mainBundle] loadNibNamed: nib owner: self options: nil];
            self = (SpecialOfferOfRestaurantCell*)[nibViews objectAtIndex: 0];
        }
    }
    return self;
}

-(IBAction)moreButtonAction:(UIButton *)sender
{
    [self.delegateSpecialOfferItem specialOfferItemDidClicked:self];
}


@end
