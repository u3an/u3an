//
//  SpecialOfferOfRestaurant.h
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpecialOfferOfRestaurantCell.h"
#import "RestaurantDetail.h"
#import "Promotions.h"
#import "RestaurantMenuListVC.h"

@interface SpecialOfferOfRestaurant : UIViewController<specialOfferItemDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *specialOfferLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *specialOfferCellScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *specialOfferMainScrollView;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (assign, nonatomic)NSInteger rating_Count;

@end
