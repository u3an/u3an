//
//  AboutRestaurantVC.h
//  U3AN
//
//  Created by Vipin on 04/02/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantDetail.h"

@interface AboutRestaurantVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UITextView *restaurantDescriptionTxtView;
@property (strong, nonatomic)RestaurantDetail *restaurantDetailObj;
@property (weak, nonatomic) IBOutlet UILabel *aboutRestaurantLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *aboutRestaurantScrollView;


@end
